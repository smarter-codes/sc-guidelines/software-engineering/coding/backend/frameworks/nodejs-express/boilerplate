# Contributing
When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with team members working on this repository before making a change.

Please note we have a set of [guidelnes](https://gitlab.com/smarter-codes/guidelines), please follow it in all your interactions with the project.

## Guidelines

**No of approvals needed for merge: 2**

Befoire you start contributing here be sure to go through following and enforce them while contributing.
- [Genreal Programming Guidelines (Essentials/Priority A)](https://gitlab.com/groups/smarter-codes/guidelines/software-engineering/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Priority%3A%3AA%20(Essential))
- [Genreal Programming Guidelines (Stronblgy Reccomended/Priority B)](https://gitlab.com/groups/smarter-codes/guidelines/software-engineering/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Priority%3A%3AA%20(Essential))
- [Code Reviewing Guidelines](https://gitlab.com/smarter-codes/guidelines/software-engineering/code-review)
- [Firebase Docs](https://firebase.google.com/docs/database)


